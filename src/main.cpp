/* Copyright (c) 2023 Thomas Bailleux
*
*  Licensed under the MIT license: https://opensource.org/licenses/MIT
*  Permission is granted to use, copy, modify, and redistribute the work.
*  Full license information available in the project LICENSE file.*/


#include <iostream>
#include <fstream>
#include <string>

// obtain app option 
#include <getopt.h>

// utf32 conversion
#include <locale>
#include <codecvt>

std::u32string to_utf32(const std::string &s);
void help();

int main(int argc, char **argv) {
    bool asci_only = false;
    bool ignore_empty = false;

    {
        int tmp = 0;
        while (tmp != -1) {
        static struct option long_options[] = {
            /* These options don’t set a flag. We distinguish them by their indices. */
            {"ascii_only",   no_argument,       0, 'a'},
            {"ignore_empty", no_argument,       0, 'i'},
            {"help",         no_argument,       0, 'h'},
            {0, 0, 0, 0}};
        /* getopt_long stores the option index here. (Not used here)*/
        int option_index = 0;

        tmp = getopt_long (argc, argv, "aih", long_options, &option_index);

        switch (tmp) {
            case 'a':
                asci_only = true;
                break;

            case 'i':
                ignore_empty = true;
                break;

            case 'h':
                help();
                return 0;
                break;

            case '?':
                /* getopt_long already printed an error message. */
                help();
                return 1;
                break;
            
            case -1:
                /* Detect the end of the options. */
                break;

            default:
                abort ();
            }
        }

    }

    signed int line_count = 0;

    for (std::string line; std::getline(std::cin, line);) {
        line_count++;
        if (!(line_count == 1)) {
            std::cout<< '\n';
        }

        if (ignore_empty && line.empty()) {
            std::cout << line;
        }
        else {
            std::u32string u32_line = to_utf32(line);
            if (asci_only && line.size() > u32_line.size()) {  // need to remove non-ascii char
                size_t tmp = 0;
                while (tmp < u32_line.size()) {
                    while (tmp < u32_line.size() && u32_line[tmp] > 127) {  // Ascii limit
                        u32_line.erase(tmp, 1);
                    }
                    while (tmp < line.size() && static_cast<int>(line[tmp]) != static_cast<int>(u32_line[tmp])) {
                        line.erase(tmp, 1);
                    }
                    tmp ++;
                }
                std::cout << line.size() << "\t" << line;
            }
            else {
                std::cout << u32_line.size() << "\t" << line;
            }
        }
    }

    //case for empty input
    if (line_count == 0) {
        if (!ignore_empty)
        {
            std::cout << "0\t";
        }
    }

    return 0;
}


std::u32string to_utf32(const std::string &s) {
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
    return conv.from_bytes(s);
}


void help() {
    std::cout << "\
Use: ccpl [OPTION]... [TEXT]\n\
Count the number of characters on each line provided TEXT.\n\
\n\
The output format of each inputed line is as follows:\n\
[CHARACTER COUNT]\\t[LINE INPUTED]\n\
\n\
Without provided TEXT, read the standard input.\n\
\n\
	-a,	--ascii-only	count only ASCII characters\n\
	-i,	--ignore-empty	ignore empty lines\n\
	-h,	--help			this short help message with available options\n\
\n\
Project homepage: <https://gitlab.com/Saufian/ccpl>\n";
}