# Copyright (c) 2023 Thomas Bailleux
#
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and redistribute the work.
# Full license information available in the project LICENSE file.

# Ccpl Makefile for compiling, testing and packaging

EXEC_NAME=ccpl
PACKAGE_DIR=package
PACKAGE_NAME=$(EXEC_NAME)

SRC_DIR=src
OBJ_DIR=obj
TEST_DIR=test
EXEC_DIR=obj

SRC= $(wildcard $(SRC_DIR)/*.cpp)
OBJ= $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
export EXEC=$(EXEC_DIR)/$(EXEC_NAME)

CC=g++
CFLAGS=-W -Wall -std=c++11
LDFLAGS=

.PHONY: test clean mrproper

# create missing folder
_dummy := $(shell mkdir -p $(OBJ_DIR) $(EXEC_DIR))

# compiling
all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -c $< -o $@ $(CFLAGS)

# testing
test: $(EXEC)
	@(cd $(TEST_DIR) && $(MAKE))

# cleanup
clean:
	rm -f $(OBJ)

mrproper: clean
	rm -f $(EXEC)

# packaging
deb: lcount.deb

lcount.deb: $(PACKAGE_DIR)/DEBIAN/control $(PACKAGE_DIR)/usr/bin/$(EXEC_NAME)
	dpkg-deb --build lcount

$(PACKAGE_DIR)/usr/bin/$(EXEC_NAME): $(EXEC)
	cp $(EXEC) $@
