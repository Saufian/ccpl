# Count characters per line

A small, Unix minded, utility that count the number of characters on each line
provided.

## Quick start

### Download

Currently, the only available version is the source code, but a _Debian_ package
is in preparation.

Get it by downloading directly
[the source code on GitLab](https://gitlab.com/Saufian/ccpl/-/archive/master/ccpl-master.zip)

Or clone the repository:

```bash
git clone https://gitlab.com/Saufian/ccpl.git
```

### Build / Run

Building the tool is as simple as running the `make` command (no other
pre-requisite):

1. unpack the source code in a folder
2. enter into this folder
3. run the `make` command

### Install and Usage

Once built, `ccpl` just needs to be copied in a folder accessible through the
_path_ in your system. You can find the executable in the `obj/` folder

Calling `ccpl` should then be straightforward.

Example:

```shell
cat filename.txt | ccpl
10  First line
9   Last line
```

Available options are:

* `-a` or `--ascii-only`: count only _ASCII_ characters
* `-i` or `--ignore-empty`: ignore empty lines
* `-h` or `--help`: a short help message with available options

## License

Copyright (c) 2023 Thomas Bailleux

This code is released under the MIT license. See the [LICENSE](LICENSE) file for
more information.

## Contact

You can contact the developer by opening an issue in the project repository.

Homepage: [Count characters per line](https://gitlab.com/Saufian/ccpl)
